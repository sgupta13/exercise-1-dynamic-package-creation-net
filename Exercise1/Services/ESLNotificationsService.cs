﻿using System;
using Silanis.ESL.SDK;
using System.Diagnostics;
using Exercise1.Models;
using Exercise1.Database;

namespace Exercise1.Services
{
    // Registers and saves the event notifications
    public class ESLNotificationsService
    {
        public void saveNotifications(ESLNotificationsModel eslNotification)
        {
            string name = eslNotification.name;
            string packageId = eslNotification.packageId;

            Exercise1.Models.NotificationEventsType enumName;
            NotificationsDatabase database = NotificationsDatabase.getInstance;

            if (Enum.TryParse(name, out enumName))
            {
                switch (enumName)
                {
                    case NotificationEventsType.PACKAGE_ACTIVATE: Trace.WriteLine("Package activated");
                        break;
                    case NotificationEventsType.PACKAGE_COMPLETE: Trace.WriteLine("Package completed");
                        break;
                    case NotificationEventsType.PACKAGE_CREATE: Trace.WriteLine("Package created");
                        break;
                    case NotificationEventsType.PACKAGE_DEACTIVATE: Trace.WriteLine("Package deactivated");
                        break;
                    case NotificationEventsType.PACKAGE_DECLINE: Trace.WriteLine("Package declined");
                        break;
                    case NotificationEventsType.PACKAGE_DELETE: Trace.WriteLine("Package deleted");
                        break;
                    case NotificationEventsType.PACKAGE_EXPIRE: Trace.WriteLine("Package expired");
                        break;
                    case NotificationEventsType.PACKAGE_OPT_OUT: Trace.WriteLine("Package opted out");
                        break;
                    case NotificationEventsType.PACKAGE_READY_FOR_COMPLETION: Trace.WriteLine("Package ready for completion");
                        break;
                    case NotificationEventsType.PACKAGE_RESTORE: Trace.WriteLine("Package restored");
                        break;
                    case NotificationEventsType.PACKAGE_TRASH: Trace.WriteLine("Package trashed");
                        break;
                    case NotificationEventsType.DOCUMENT_SIGNED: Trace.WriteLine("Document signed");
                        break;
                    case NotificationEventsType.ROLE_REASSIGN: Trace.WriteLine("Role reassigned activated");
                        break;
                }
                Trace.WriteLine("Saving to DB..");
                database.saveToDatabase(packageId, name);
            }
            else
            {
                throw new SystemException("Unhandled eslNotification type " + name);
            }
        }
    }

}