﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Exercise1.Models;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Diagnostics;
using Exercise1.Controllers;
using Silanis.ESL.SDK;
using Silanis.ESL.SDK.Builder;
using System.Text;
using Exercise1.Database;
using Exercise1.Services;

namespace Exercise1.Controllers
{
    public class ESLNotificationsController : Controller
    {
        static NotificationsDatabase database = NotificationsDatabase.getInstance;

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

  
        [HttpPost]
        public void Index(ESLNotificationsModel notification)
        {
            //TODO: Uncomment the logging statement below and deploy your application to the cloud and see it in action!
            //Trace.WriteLine("Notification " + notification.name +  " for package " + notification.packageId + " received");

            ESLNotificationsService svc = new ESLNotificationsService();
            svc.saveNotifications(notification);

            Response.ClearContent();
            Response.StatusCode = (int)HttpStatusCode.NoContent;
        }

        //Displays the e-SignLive event notifications
        //Displays the status of the packages
        [HttpGet]
        public ActionResult Dashboard()
        {
            Dictionary<String, String> updatedDatabase = database.getFromDatabase();
            if (updatedDatabase.Count == 0)
            {
                Trace.WriteLine("No packages to view..Please create a package.");
            }

            Trace.WriteLine("DB entries below:");
            foreach (var entry in updatedDatabase)
            {
                Trace.WriteLine("Package ID: " + entry.Key + " Status: " + entry.Value);
            }

            ViewData["updatedDatabase"] = updatedDatabase;
            return View();
        }
    }
}